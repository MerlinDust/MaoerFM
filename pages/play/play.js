let audio =  wx.createInnerAudioContext()

Page({

  data: {
    toView: 'green',
    commentList:[],
    soundMessage:{},
    barrage:{},
    comment:{},
    isPlay:true,
    scrollLeft:0

  },

  scrollToLeft(){
    for(let i=this.data.scrollLeft;i>0;i-=10)
      this.setData({scrollLeft:i})
    this.setData({scrollLeft:0})
  },

  scrollToRight(){
    for(let i=this.data.scrollLeft;i<380;i+=10)
      this.setData({scrollLeft:i})
    // this.setData({scrollLeft:380})
  },

  nowPlay(){
    
    if( this.data.isPlay){
      audio.pause();
      this.setData({isPlay:false});
    }else{
      audio.play();
      this.setData({isPlay:true});
    }
  },

  barrageContent:function(e){
    console.log(e);
    this.data.barrage = e.detail.value;
    console.log(this.data.barrage);
  },

  commentContent:function(e){
    console.log(e);
    this.data.comment = e.detail.value;
    console.log(this.data.comment);
  },

  

  favoriteClick(options){
    console.log(options);
    let {username,soundid,soundstr,soundimg} = options.currentTarget.dataset;
    let flag;
    wx.request({
      url: 'http://localhost:8080/MaoerFM/setFavoriteList',
      data: {username,soundid,soundstr,soundimg},
      header: {'content-type':'application/json'},
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: (result)=>{
        console.log(result);
        let flag = result.data;

        if(flag){
          wx.showToast({
            title: '成功添加到喜欢',
            icon: 'success',
            duration: 2000//持续的时间
          })
        }else{
          wx.showToast({
            title: '您已成功添加',
            icon: 'success',
            duration: 2000//持续的时间
          })
        }
      },

    });


  },

  onLoad:function(options){
    
    // 获取剧字幕
    // https://www.missevan.com/sound/getdm?soundid=2203441
    //获取评论
    wx.request({
      url: `https://www.missevan.com/site/getrecommend?type=1&e_id=${options.id}&c_num=3`,
      data: {},
      header: {'content-type':'application/json'},
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: (result)=>{
        console.log(result);
        let arr = [];
        result.data.info.forEach(element => {
          let {username,ctime,icon,comment_content,subcomments,sub_comment_num} = element;
          arr.push({username,ctime,icon,comment_content});
          if(sub_comment_num == 0);
          else if(sub_comment_num == 1){
              console.log(subcomments);
              let [{username,ctime,icon,comment_content}] = subcomments;
              arr.push({username,ctime,icon,comment_content});
            }else{
              subcomments.forEach(item =>{
              let {username,ctime,icon,comment_content} = item;
              arr.push({username,ctime,icon,comment_content});
              })
            } 
        });
        console.log(arr);
        this.setData({
          commentList:arr,
        });
      },

    });


    wx.request({
      url: `https://www.missevan.com/sound/getsound?soundid=${options.id}`,
      data: {},
      header: {'content-type':'application/json'},
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: (result)=>{
        console.log(result);
        let {id,soundstr,intro,soundurl,view_count,comment_count,favorite_count,front_cover} = result.data.info.sound;
        // let {soundstr,intro,soundurl} = result.data.info.sound;
        let arr = intro.split("<p>");
        intro = arr.join("");
        arr = intro.split("</p>");
        intro = arr.join("\n");
        arr = intro.split("</br>");
        intro = arr.join("");
        arr = intro.split("<br />");
        intro = arr.join("");
        console.log(intro);
        audio.src = soundurl;
        this.setData({
          soundMessage:{id,soundstr,intro,soundurl,view_count,comment_count,favorite_count,front_cover},
          // soundMessage:{soundstr,intro,soundurl},
        });
        console.log({soundstr,intro,soundurl,view_count,comment_count,favorite_count,front_cover});
      },
    });

    audio.play();
  },


  onShareAppMessage() {
    return {
      title: this.data.soundMessage.soundstr,
      path: 'page/component/pages/scroll-view/scroll-view'
    }
  },

  

  upper(e) {
    console.log(e)
  },

  lower(e) {
    console.log(e)
  },

  scroll(e) {
    console.log(e)
  },

  scrollToTop() {
    this.setAction({
      scrollTop: 0
    })
  },

  tap() {
    for (let i = 0; i < order.length; ++i) {
      if (order[i] === this.data.toView) {
        this.setData({
          toView: order[i + 1],
          scrollTop: (i + 1) * 200
        })
        break
      }
    }
  },

  tapMove() {
    this.setData({
      scrollTop: this.data.scrollTop + 10
    })
  }
})