// pages/search/search.js
Page({
  data: {
    list:[],
    barrage:{}
  },
  onLoad() {
  },
  
  barrageContent: function(e) {
    this.data.barrage = e.detail.value;
    
  },
  handleSearch: function(){
    console.log(keyword);
    let keyword = this.data.barrage;
    wx.request({
      url: `https://www.missevan.com/sound/getsearch?s=${keyword}&p=1&type=3&page_size=30`,
      header: {'content-type':'application/json'},
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: (res)=>{
        let arr = [];
        res.data.info.Datas.forEach((item,index)=>{
            let {id,username,front_cover,soundstr} = item;
            arr.push({
                id,username,front_cover,soundstr
            })
        })
        console.log(arr);
        this.setData({
            list:arr
        })
      },
    });
  },
  handleClick(event){
    let id = event.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/play/play'+"?id="+id
    })
  }
})