// pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrls: [
      '/images/banner3.jpg',
      '/images/banner4.jpg',
      '/images/banner5.jpg',
      '/images/banner6.jpg',
      '/images/banner7.jpg',
      '/images/banner8.jpg'
    ],
    indicatorDots: true,
    autoplay: true,
    circular: true,
    interval: 5000,
    duration: 500,

    arr:[],
    week:[],
    bang:[]
  },
  jumpSearch: function(){
    wx.navigateTo({
      url: '/pages/search/search',
    })
  },
  jumpVideo: function(){
    wx.navigateTo({
      url: '/pages/mine/mine',
    })
  },
  onLoad: function (options) {
    let url = "https://www.missevan.com/sound/getsoundlike?sound_id=2179531&type=15"
    let urll = "https://www.missevan.com/dramaapi/summerdrama"
    let urlll = "https://www.missevan.com/reward/drama-reward-rank?period=1&page=1&page_size=6"
    console.log("onload");
    wx.request({
        url,
        header: {'content-type':'application/json'},
        method: 'GET',
        dataType: 'json',
        responseType: 'text',
        success: (res)=>{
            let info = res.data.info.dramas;
            let arr = [];
            info.forEach((item,index)=>{
                let {id,name,front_cover} = item;
                arr.push({
                    id,name,front_cover
                })
            })
            /*console.log(arr);*/
            this.setData({
                arr:arr
            })
        }
    });
    wx.request({
      url: urll,
      header: {'content-type':'application/json'},
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: (res)=>{
          let arr = [];
          res.data.info.forEach((item,index)=>{
              let [{id,name,cover}] = item;
              arr.push({
                  id,name,cover
              })
          })
          this.setData({
              week:[arr[0],arr[1],arr[2],arr[3]]
          })
      }
    });
    wx.request({
    url: urlll,
    header: {'content-type':'application/json'},
    method: 'GET',
    dataType: 'json',
    responseType: 'text',
    success: (res)=>{
        let arr = [];
        res.data.info.ranks.Datas.forEach((item,index)=>{
            let {id,name,cover} = item;
            arr.push({
                id,name,cover
            })
        })
        this.setData({
            bang:arr
        })
      }
    });
  },
  handleClick(event){
    let id = event.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/detail/detail'+"?id="+id
    })
  }
})