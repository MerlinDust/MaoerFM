// pages/list/list.js
Page({
  data: {
    jianjie:{},
    arr:[],
    cv:[],
  },
  onLoad: function (options) {
    let url = `https://www.missevan.com/dramaapi/getdrama?drama_id=${options.id}`
    console.log("onload");
    wx.request({
        url,
        header: {'content-type':'application/json'},
        method: 'GET',
        dataType: 'json',
        responseType: 'text',
        success: (res)=>{
            let {id,name,cover,author,abstract,type,catalog_name} = res.data.info.drama;
            console.log(res.data.info.drama);
            let arr = abstract.split("<p>");
            abstract = arr.join("");
            arr = abstract.split("</p>");
            abstract = arr.join("\n");
            arr = abstract.split("</br>");
            abstract = arr.join("");
            arr = abstract.split("<br/>");
            abstract = arr.join("");
            arr = abstract.split("&nbsp;");
            abstract = arr.join("");
            console.log(abstract);
            
            this.setData({
                jianjie: {id,name,cover,author,abstract,type,catalog_name},
            });
        }
    });
    wx.request({
        url,
        header: {'content-type':'application/json'},
        method: 'GET',
        dataType: 'json',
        responseType: 'text',
        success: (res)=>{
            let info = res.data.info.episodes.episode;
            let arr = [];
            info.forEach((item,index)=>{
                let {id,name,sound_id} = item;
                arr.push({
                    id,name,sound_id
                })
            })
            /*console.log(arr);*/
            this.setData({
                arr:arr
            })
        }
    });
    wx.request({
        url,
        header: {'content-type':'application/json'},
        method: 'GET',
        dataType: 'json',
        responseType: 'text',
        success: (res)=>{
            let info = res.data.info.cvs;
            let arr = [];
            info.forEach((element)=>{
                let {cv_info,character} = element;
                    let {icon,name,group} = cv_info;
                    arr.push({icon,name,group,character});
            })
            /*console.log(arr);*/
            this.setData({
                cv:arr
            })
        }
    });
  },
  handleeClick(event){
    let id = event.currentTarget.dataset.id;
    console.log(id);
    wx.navigateTo({
      url: '/pages/play/play'+"?id="+id
    })
  }
})